# sfc-cli

🛠 A component scaffolding for Mxchip front-end team. 

## Installation

Prerequisites: [Node.js](https://nodejs.org/en/) (>=6.x, 8.x preferred), npm version 3+ and [Git](https://git-scm.com/).

``` bash
$ npm install @mxchip/sfc-cli -g
```

## Usage

``` bash
# Get help
$ mx --help
# List components
$ mx list
# Down components
$ ora down <component_name>
```

Example:

``` bash
# List
$ mx l
# Down
$ mx d test
```

The above command pulls the template from [FEMxchip](https://gitlab.com/femxchip), prompts for some information, and download the component at `./src/components/component_name` .

## License
[MIT](./LICENSE)